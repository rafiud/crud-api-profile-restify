const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  user_id: {
    type: String,
    require: true
  },
  nama: {
    type: String,
    require: true
  },
  kelas: {
    type: String,
    required: true
  },
  alamat: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  isVerified: {
    type: Boolean,
    default: false
  }
});

mongoose.model('user', userSchema);