const restify = require('restify');
const project = require('./package.json');
const userController = require('./controllers/user.controller');

function AppServer() {
  // create server
  this.server = restify.createServer({
    name: `${project.name}-server`,
    version: project.version
  });
  
  this.server.use(restify.plugins.bodyParser());
  this.server.use(restify.plugins.queryParser());

  // root
  this.server.get('/', (req, res) => {
    res.send({ success:true, data:'index', message:'This service is running properly', code:200 });
  });

  // user route
  this.server.get('/user', userController.getHandler);
  this.server.get('/user/:user_id', userController.getById);
  this.server.get('/search', userController.searchBy);
  this.server.put('/accept/:user_id', userController.acceptUser);
  this.server.put('/decline/:user_id', userController.declineUser);
  this.server.post('/user', userController.postHandler);
  this.server.put('/user/:user_id', userController.putHandler);
  this.server.del('/user/:user_id', userController.deleteHandler);
}

module.exports = AppServer;