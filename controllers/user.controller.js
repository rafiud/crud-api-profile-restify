const uuidv4 = require('uuid/v4');
const mongoose = require('mongoose');
const model = require('../models/user.model');
const User = mongoose.model('user');
const response = require('../helper/wrapper');
const { ERROR: httpError } = require('../helper/httpError');
const files  = require('../helper/files');
const fs = require('fs');

const userController = {
  getHandler : (req, res) => {
    User.find((err,  value) => {
      if (err) {
        return response.wrapper_error(res, httpError.INTERNAL_ERROR, 'An error has occurred');
      }

      if (value.length > 0) {
        response.wrapper_success(res, 200, 'Request has been proceseed', value);
      } else {
        response.wrapper_error(res, httpError.NOT_FOUND, 'User not found');
      }

    });
  },
  searchBy : (req, res) => {
    let payload = {
      // "$or": [{
          nama: new RegExp(req.query.nama)
      // }, {
      //   alamat: req.query.alamat
      //     // alamat: new RegExp(req.query.alamat)
      // }, {
      //   kelas: req.query.kelas
      //     // kelas: new RegExp(req.query.kelas)
      // }]
    }
    User.find(payload, (err,  value) => {
      if (err) {
        return response.wrapper_error(res, httpError.INTERNAL_ERROR, 'An error has occurred');
      }

      if (value.length > 0) {
        response.wrapper_success(res, 200, 'Request has been proceseed', value);
      } else {
        response.wrapper_error(res, httpError.NOT_FOUND, 'User not found');
      }

    });
  },
  acceptUser : (req,res) => {
    const isTrue = true;
    let payload = {
      user_id: req.params.user_id
    }
    let body = {
      isVerified : isTrue
    }
    User.findOneAndUpdate(payload,body, (err, value) => {
      User.findOne(payload,(err,value) => {
        if (err) {
          return res.status(500).send({'error':'An error has occurred'});
        }
  
        if (value != null) {
          response.wrapper_success(res, 202, 'User has been verified', value);
        } else {
          response.wrapper_error(res, httpError.INTERNAL_ERROR, 'Failed to verified user');
        }
      })

    });
  },
  
  declineUser : (req,res) => {
    const isFalse = false;
    let payload = {
      user_id: req.params.user_id
    }
    let body = {
      isVerified : isFalse
    }
    User.findOneAndUpdate(payload,body, (err, value) => {
      User.findOne(payload,(err,value) => {
        if (err) {
          return res.status(500).send({'error':'An error has occurred'});
        }
  
        if (value != null) {
          response.wrapper_success(res, 202, 'User has been rejected', value);
        } else {
          response.wrapper_error(res, httpError.INTERNAL_ERROR, 'Failed to reject user');
        }
      })

    });
  },

  getById : (req, res) => {
    let payload = {
        user_id: req.params.user_id
    }
    User.findOne(payload,(err,value) => {
        if (err) {
            return res.status(500).send({'error':'An error has occurred'});
          }
    
          if (value) {
            response.wrapper_success(res, 200, 'Request has been process', value);
          } else {
            response.wrapper_error(res, httpError.NOT_FOUND, 'Data user is not found');
        }
    
    })
  },

  postHandler : (req, res) => {

    const objFile = req.files;
    const file = objFile['photo'];

    let fileName = file.name;
    let filePath = file.path;
    const fileData = fs.readFileSync(filePath);

    const newPath = files.to('/uploads/' + fileName);
    fs.writeFileSync(newPath, fileData);
    
    let payload = {
      user_id: uuidv4(),
      nama: req.body.nama,
      kelas: req.body.kelas,
      alamat: req.body.alamat,
      photo: newPath
    }

    User.create(payload, (err, value) => {
      if (err) {
        return response.wrapper_error(res, httpError.INTERNAL_ERROR, 'An error has occurred');
      }

      response.wrapper_success(res, 201, 'User has been inserted', value);
    });
  },

  putHandler : (req, res) => {
    const objFile = req.files;
    const file = objFile['photo'];

    let fileName = file.name;
    let filePath = file.path;
    const fileData = fs.readFileSync(filePath);

    const newPath = files.to('/uploads/' + fileName);
    fs.writeFileSync(newPath, fileData);
    
    let payload = {
      user_id: req.params.user_id,
    }

    let body = {
        nama: req.body.nama,
        kelas: req.body.kelas,
        alamat: req.body.alamat,
        photo: newPath
    }

    User.findOneAndUpdate(payload,body, (err, value) => {
      User.findOne(payload,(err,value) => {
        if (err) {
          return res.status(500).send({'error':'An error has occurred'});
        }
  
        if (value != null) {
          response.wrapper_success(res, 202, 'User has been updated', value);
        } else {
          response.wrapper_error(res, httpError.INTERNAL_ERROR, 'Failed to update user');
        }
      })

    });
  },

  deleteHandler : (req, res) => {
    let payload = {
      user_id : req.params.user_id
    }

    User.findOneAndRemove(payload, (err, value) => {
      if (err) {
        return response.wrapper_error(res, httpError.INTERNAL_ERROR, 'An error has occurred');
      }

      if (value != null) {
        res.send({
          'code': 204,
          'success': 'true',
          'message': `User ${value.nama} has been deleted`
        });
      } else {
        response.wrapper_error(res, httpError.INTERNAL_ERROR, 'Failed to delete user');
      }
    });
  }
}

module.exports = userController;